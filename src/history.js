//Creating and exporting Browser's history for the application
import { createBrowserHistory } from 'history'

export default createBrowserHistory()