//Loads the state from local browser Storage returning it as a JSON
export const loadState = () => {
	try {
		const serializedState = localStorage.getItem('state')
		if(serializedState === null) {
			return undefined
		}
		return JSON.parse(serializedState)
	} catch (err) {
		return undefined
	}
}

//Saves the state into local browser Storage 
export const saveState = (state) => {
	try {
		const serializedState = JSON.stringify(state)
		localStorage.setItem('state', serializedState)
	} catch (err) {
		//Ignore write errs
	}
}