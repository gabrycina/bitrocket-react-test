import React from 'react'
import { Field, reduxForm, SubmissionError } from 'redux-form'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom'
import { submitAction } from './actions'
import history from '../../history'
import store from '../../store'
import {
	ParentForm,
	FormMainBody,
	Button
} from '../../styles/elements'

const submit = ({ firstName, lastName, email, password }) => {
    
	let error = {}
	let isError = false

	//Some simple examples of errors
	if(firstName.trim() === '') {
		error.firstName = 'Required'
		isError = true
	}

	if(lastName.trim() === '') {
		error.lastName = 'Required'
		isError = true
	}

	if(email.trim() === '') {
		error.email = 'Required'
		isError = true
	}

	//Throws error
	if(isError) {
		throw new SubmissionError(error)
	} else {
		//Submits form data to local storage (signing up the user)
		store.dispatch(submitAction({firstName, lastName, email, password}))
		//Redirect to the welcome page
		history.push('/welcome')	
	}
}

//Render field is called for every Field Component in SignUpFormFunc
const renderField = ({ type, input, meta: { touched, error }, placeholder }) => (
	<FormMainBody className="input-row">
		<input {...input} type={type} placeholder={placeholder} required/>
		{touched && error && 
		<span className="error">{error}</span>}
	</FormMainBody>
)

//Main Component - Form
const SignUpFormFunc = ({ handleSubmit }) => (
	<ParentForm onSubmit={handleSubmit(submit)}>
		<Field name="firstName" component={renderField} type="text" placeholder={'First Name'}/>
		<Field name="lastName" component={renderField} type="text" placeholder={'Last Name'}/>
		<Field name="email" component={renderField} type="email" placeholder={'Email'}/>
		<Field name="password" component={renderField} type="password" placeholder={'Password'}/>
		<Button type="submit">Submit</Button>
	</ParentForm>	
)

//Props types definition
SignUpFormFunc.propTypes = {
	handleSubmit: PropTypes.func,
	submitAction: PropTypes.func
}

renderField.propTypes = {
	type: PropTypes.string,
	label: PropTypes.string,
	input: PropTypes.object,
	meta: PropTypes.object,
	placeholder: PropTypes.string
}

const SignUpForm = reduxForm({
	form: 'signup', //unique name for this form,
})(SignUpFormFunc)

export default withRouter(SignUpForm)