import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'

import rootReducer from './reducers'
import root from './sagas/sagas'

const sagaMiddleware = createSagaMiddleware()

const middlewares = applyMiddleware(sagaMiddleware)
const devToolsMiddleware = window.__REDUX_DEVTOOLS_EXTENSION__()

const store = createStore(
	rootReducer, 
	compose(
		middlewares,
		devToolsMiddleware
	)
)

sagaMiddleware.run(root)

export default store