import React from 'react'
import ReactDOM from 'react-dom'
import { connect, Provider } from 'react-redux'
import { Router, Route, Redirect, Switch } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import * as serviceWorker from './serviceWorker'

//Redux store import
import store from './store'

//Components import
import Welcome from './components/Welcome'
import SignUpForm from './components/SignUpForm/SignUpForm'

//Browser History import
import history from './history'

//SignUpForm's action import
import { submitAction } from './components/SignUpForm/actions'

function mapDispatchToProps (dispatch) {
	return bindActionCreators(
		{ submitAction },
		dispatch
	)
}

ReactDOM.render(
	<Provider store={store}>
		<Router history={history}>
			<Switch>
				<Route exact path="/" render={() => ( <Redirect to="/signup"/> )}/>
				<Route path='/signup' render={() => <SignUpForm />} />
				<Route path='/welcome' render={() => <Welcome /> } />
			</Switch>
		</Router>
	</Provider>,
	document.getElementById('root')
)

serviceWorker.unregister()

export default connect(null, mapDispatchToProps)(SignUpForm)


