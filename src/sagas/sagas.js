import { takeEvery, fork, call, put, all } from 'redux-saga/effects'
import { startSubmit, stopSubmit } from 'redux-form'
import { saveState } from '../localStorage'

//Called for every form submission, it passes data to the submitToLocal function
function* callSubmit(action) {
	yield put(startSubmit('signup'))
	const result = yield call(submitToLocal, action.data)
	yield put({ type: 'REQUEST_SUCCESSFUL', result }) 
	yield put(stopSubmit('signup'))
}

//Starts to listen for any sign up form submission
function* submitSaga() {
	yield takeEvery('REQUEST_SUBMIT', callSubmit)
}

//Saves data to local browser storage
function submitToLocal(data) {
	let response = saveState(data)
	return response
}

export default function* root() {
	yield all(
		[
			fork(submitSaga)
		]
	)
}