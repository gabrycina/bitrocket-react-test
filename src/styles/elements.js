import styled from 'styled-components'

const ParentForm = styled.form`
  display: grid;
  grid-template-columns;1fr;
  grid-template-rows:auto;
  margin: auto; 
    *{
      margin: auto;
      padding: 0.5rem 1rem;
      display:grid
    }

`
const HeaderForm = styled.div`
  > p {
    font-size: 1.5em;
    font-weight: 600;
  }
`
const FormMainBody = styled.div`
  grid-template-columns: 1fr;
  grid-template-rows: 1fr 1fr;
  input {
    width: 100%;
    height: 29px;
    border-radius: 4px;
    position: relative;
    background-color: rgba(255,255,255,0.3);
    transition: 0.3s all;
  }
  .input:hover {
    background-color: rgba(255, 255, 255, 0.45);
    box-shadow: 0px 4px 20px 0px rgba(0, 0, 0, 0.05);
  }
`
const Button = styled.button`
	box-shadow: 4px 6px 12px -8px #000000;
	background-color:#44c767;
	border-radius:20px;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:Arial;
	font-size:20px;
	padding:11px 76px;
	text-decoration:none;
    text-shadow:1px 2px 5px #000000;
    margin: auto; 
    width: 288px;

    :hover {
	    background-color:#5cbf2a;
    }

    :active {
    	position:relative;
	    top:1px;
    }
`

export { ParentForm, HeaderForm, FormMainBody, Button }
