module.exports = {
  stories: ['../src/**/*.stories.(js|mdx)'],
  addons:[
    '@storybook/addon-actions',
    '@storybook/addon-link',
    '@storybook/addon-events',
    '@storybook/addon-notes',
    '@storybook/addon-options',
    '@storybook/addon-knobs',
    '@storybook/addon-backgrounds',
    '@storybook/addon-ally',
    '@storybook/addon-jest',
    {
      name: '@storybook/addon-docs',
      options: { configureJSX: true },
    },
  ]
}
