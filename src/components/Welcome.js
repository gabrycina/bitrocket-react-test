import React, { Component } from 'react'
import { loadState } from '../localStorage'
class Welcome extends Component{
	render() {
		const state = loadState()
		const firstName = state.firstName

		return(
			<div>
				<h1>Welcome {firstName}!</h1>
			</div>
		)
	}
}

export default Welcome